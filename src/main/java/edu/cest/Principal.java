package edu.cest;

import java.awt.List;
import java.util.ArrayList;

import java.util.Arrays;
import edu.cest.cadastro.Agencia;
import edu.cest.cadastro.endereco.Cidade;
import edu.cest.conta.ContaCorrente;
import edu.cest.conta.ContaPoupanca;

/**
 * Classe para inicializacao do projeto,
 * com o objetivo de rodar a aplicacao via console
 * @author jwalker
 *
 */
public class Principal {
	//TODO - Implemente o inicio do programa
	//TODO - Crie duas ou mais agencias
	//TODO - Insira contas completas
	//TODO - Corrija os pacotes onde estiver errado
	//TODO - Corrija o que for necessário para atender os padrões
	//TODO - Ao fim imprima o total do saldo de todas as contas de cada agencia, seguindo os padrões já explicados em sala
	
		// criei um parametro pra chamar a ContaCorrente
	    public static void main(String[] args, ContaCorrente cc, ContaPoupanca cp, double saldo, double valor, int numeroDeCC, int numeroDeCP, List listaCC) {
	
	    	
	    // Agencia 1

		Agencia ag1 = new Agencia("0337");
		
		// agencia 2
		Agencia ag2 = new Agencia("4015");
		

	    
	    
	    ArrayList <Agencia> listaAgencias = new ArrayList<Agencia>(); // listando agencias :)
		listaAgencias.add(ag1);
		listaAgencias.add(ag2);

	    
	

	    // Conta Corrente
	  
	    
	    ArrayList <ContaCorrente> listCC = new ArrayList<ContaCorrente>();
		
	      ContaCorrente cc = new ContaCorrente();
	        cc.setNumeroConta ("Conta: 0011");
		     cc.setSaldoC (13883.00);
	    

	      ContaCorrente cc2 = new ContaCorrente();
	    	cc2.setNumeroConta("Conta: 002");
		    cc2.setSaldoC(240.00);

		
	    // Conta Poupanca
		
	  ContaPoupanca cp = new ContaPoupanca();
		cp.setNumeroConta("Conta: 0110");
		cp.setSaldo(50050.00);
		
		ContaPoupanca cp2 = new ContaPoupanca();
		cp2.setNumeroConta("Conta: 016");
		cp2.setSaldo(9900.00);
		
	 // CONTA CORRENTE DA AGENCIA 1
    // ADICIONADO A CONTA CORRENTE
       ArrayList <ContaCorrente> listaCC = new ArrayList <ContaCorrente>();
		listaCC.add(cc);
		ag1.setNumeroDeCC(ag1.getNumeroDeCC () + 1);
		ag1.setListaCC(listaCC);
		// CONTA POUPANCA DA AGENCIA 1
		// ADICIONADO A CONTA POUPANCA
		ArrayList <ContaPoupanca> listaCP = new ArrayList <ContaPoupanca>();
		listaCP.add(cp);
		ag1.setNumeroDeCP(ag1.getNumeroDeCP () + 1);
		ag1.setListaCP(listaCP);

        // CONTA CORRENTE DA AGENCIA 2
		// ADICIONADO A CONTA CORRENTE
		ArrayList <ContaCorrente> listaCCag2 = new ArrayList <ContaCorrente>();
		listaCCag2.add(cc2);
		ag2.setNumeroDeCC(ag2.getNumeroDeCC () + 1);
		ag2.setListaCC(listaCCag2);
		
	    // CONTA POUPANCA DA AGENCIA 2
		// ADICIONADO A CONTA POUPANCA
		ArrayList <ContaPoupanca> listaCPag2 = new ArrayList <ContaPoupanca>();
		listaCPag2.add(cp2);
		ag2.setNumeroDeCP(ag2.getNumeroDeCP () + 1);
		ag2.setListaCP(listaCPag2);

	

		
		
		// TODO - Implemente de modo que haja uma lista das agencias
		// TODO - Implemente de modo que o System.out ao chamar o objeto agencia, imprima os dados como Numero da Agencia e quantidade de contas
	
		// Resultado da Agencia 1
	 	
	    
	
		for (Agencia age: listaAgencias) {
			System.out.println("Agencia - Dados");
			System.out.println(age.getCodAgencia());
			System.out.println("Numero Conta corrente:");
			System.out.println(age.getNumeroDeCC());
			System.out.println("Numero Conta poupança:");
			System.out.println(age.getNumeroDeCP());
			System.out.println("----------------");
		}
	}
}

		
	
		
		
		
		
		
		
	}
}
