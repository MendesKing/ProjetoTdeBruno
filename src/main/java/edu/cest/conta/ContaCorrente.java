package edu.cest.conta;
import java.util.Random;


public class ContaCorrente {

	/**
	 * TODO - Corrija deixando os atributos protegidos e acessiveis somente de
	 * acordo com os padroes
	 */
	Random rand = new Random();
	private double saldo = 0;
	private double saldoChequeEspecial = rand.nextDouble() * 1000; //NAO ALTERE ESSA CHAMADA
	private String numeroConta;
	
	
	
	

	
public ContaCorrente(String NumCC){
    this.numeroConta = NumCC;
    
}
	

	public void setNum(String numeroConta) {
		
		this.numeroConta = numeroConta();
		
	}
	
	
	
	private String numeroConta() {
		
	
		
		
		return this.numeroConta;
	}



	/**
	 * TODO - Implemente o saque para subtrair o valor do saldo.
	 * TODO - Aten&ccedil;&atilde;o o saque s&oacute; pode ser efetuado se tiver saldo suficiente na conta <i>MAS</i>, verifique o cheque especial
	 * @param saque 
	 * @return - Saldo apos o saque
	 */
	public double saque(double valor) {
		//TODO - Me implemente corretamente
		if(valor <= this.saldo){
        this.saldo = this.saldo - valor;
             
			
		}
		return this.saldo;		
		
	}
	
	

	
	
	/**
	 * TODO - Implemente o dep&oacute;sito
	 * TODO - Aten&ccedil;&atilde;o o dep&oacute;sito n&atilde;o pode ser de valor negativo
	 */
	public double deposito(double valor) {
		//TODO - Me implemente corretamente
		
			if (valor >= 0) {
	
				this.saldo += valor;
            }else{
            println("Deposito não efetuado");
    
            }
		return this.saldo;
	}



	public double getSaldo() {
		return saldo;
	}



	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}



	public double getSaldoChequeEspecial() {
		return saldo+saldoChequeEspecial;
	}



	public void setSaldoChequeEspecial(double saldoChequeEspecial) {
		this.saldoChequeEspecial = saldoChequeEspecial;
	}



	public String getNumeroConta() {
		return numeroConta;
	}



	public void setNumeroConta(String numeroConta) {
		this.numeroConta = numeroConta;
	}



	
	
	
	

}
