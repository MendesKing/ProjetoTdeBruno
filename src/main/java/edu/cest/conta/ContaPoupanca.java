package edu.cest.conta;

public class ContaPoupanca {
	/**
	 *  TODO - Corrija deixando os atributos protegidos e acessiveis somente de
	 *  acordo com os padroes
	 */
   private double saldo = 0;
	private String numeroConta;
	 

	
	/**
	 * TODO - Implemente o saque para subtrair o valor do saldo.
	 * TODO - Atenção o saque só pode ser efetuado se tiver saldo suficiente na conta
	 * @param d 
	 * @return - Saldo apos o saque
	 */
	
	// Numero da Conta
	public void numeroConta(String numeroConta) {
		// TODO Auto-generated method stub
		this.numeroConta=numeroConta;
		
		
	}
	
	public String numeroConta(){
		return numeroConta;
		
	}
	
	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	

	
	
	
	// Saque
	public double saque(double valor) {
		//TODO - Me implemente corretamente
		
		if (valor <= this.saldo) {
			this.saldo = this.saldo - valor;
			
		}
		
		
		return this.saldo;
	}
	
	/**
	 * TODO - Implemente o depósito
	 * TODO - Atenção o depósito não pode ser de valor negativo
	 */
	
	// Deposito
	public double deposito(double valorDepositado) {
		//TODO - Me implemente corretamente
		if(valorDepositado >= 0) {
			this.saldo = this.saldo + valorDepositado;
			
		}
		
		return this.saldo;
	}

	
	
}
