package edu.cest.cadastro.endereco;

/**
 * 
 * @author jwalker
 *
 */
public class Cidade {
	private UF estado;
	private String strCidade;
	private int codCidade;
	

	// TODO - Implemente os getters somente, desse modo o usuário não poderar
	// alterar os dados da cidade
	// Que conceito estamos aplicando?
	
	/**
	 * 
	 * @param estado - Objeto UF identificando a que estado a cidade pertence
	 * @param strCidade - Nome da Cidade
	 * @param codCidade - Codigo com 3 letras da Cidade
	 */
	public Cidade(UF estado, String strCidade, int codCidade) {
		super();
		this.estado = estado;
		this.strCidade = strCidade;
		this.codCidade = codCidade;
	}
	


	


	public UF getUF(UF estado) {
		
		return this.estado;
		
	}
	public String getCid() {
		
		return this.strCidade;
		
	}
	
	public int getCod() {
	
		return this.codCidade;
		
	}

	
	
}
