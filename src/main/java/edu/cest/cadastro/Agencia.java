package edu.cest.cadastro;

import java.util.ArrayList;

import edu.cest.cadastro.endereco.Cidade;
import edu.cest.conta.ContaCorrente;
import edu.cest.conta.ContaPoupanca;
/**
 * Classe com registros da agencia
 * 
 * @author jwalker
 *
 */
public class Agencia {

	/**
	 * TODO - Implemente os getters e setters de todos os atributos
	 */
	private String codAgencia = null;
	private String strEndereco = null;
	private Cidade cidade;
	private Agencia ag;
	
	public void setCod(String codAgencia) {
		
		this.codAgencia = codAgencia;
		
	}
	
	public String getCod() {
		
		return this.codAgencia;
	}
	
	
	
public void setEnd(String strEndereco) {
		
		this.strEndereco = strEndereco;
		
	}
	
	public String getEnd() {
		
		return this.strEndereco;
	}
	
	
public void setCid(Cidade cidade) {
	
	this.cidade = cidade;
	
}
	
	public Cidade getCid() {
		
		return this.cidade;
	}
	
	
	// TODO - Implemente o incremento de acordo com o tipo de conta
	private int numeroDeCC = 0;
	private int numeroDeCP = 0;

	public void setCC(int numeroDeCC) {
		
		this.numeroDeCC = numeroDeCC;
		
	}
	
	public int getCC() {
		
		return this.numeroDeCC;
		
	}
	
	
	public void setCP(int numeroDeCP) {
		
		this.numeroDeCP =  numeroDeCP;
		
	}
	
	public int getCP() {
		
		return this.numeroDeCP;
		
	}
	
	
	/**
	 * Lista de Contas Corrente TODO - Implemente de modo a evitar a insercao de
	 * coisas diferentes de CC
	 */
	private ArrayList <ContaCorrente> listaCC = new ArrayList <ContaCorrente>();

	
	public void contaCC(ContaCorrente cc) {
		listaCC.add(cc);
		
		
	}
	
	
	//Agencia  cod1 = new Agencia(listaCC);
	
	
	
	/**
	 * Lista de Contas Poupanca TODO - Implemente de modo a evitar a insercao de
	 * coisas diferentes de CP
	 */
	private ArrayList <ContaPoupanca> listaCP = new ArrayList<ContaPoupanca>();
	
	public void contaPP(ContaPoupanca cp) {
		listaCP.add(cp);		
	}
	
	
	//Agencia cod2 = new Agencia(listaCP);
	
	
	
		
	/**
	 * TODO - Implemente o construtor para que seja passado o codigo da Agencia
	 * 
	 * @param ag
	 *            - Codigo da Agencia Bancaria
	 */
	public Agencia(Agencia ag) {
		// TODO - Implemente para que seja passado o codigo da Agencia
		
	this.setAg(ag);
	
		
		
	}

	public Agencia(String string) {
	
		// TODO Auto-generated constructor stub
		
		
	}
	
	

	public Agencia getAg() {
		return ag;
	}
	

	public void setAg(Agencia ag) {
		this.ag = ag;
	}
	
	
	
	
}
